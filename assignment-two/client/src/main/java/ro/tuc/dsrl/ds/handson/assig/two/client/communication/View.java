package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JPanel contentPane;
    private JTextField year;
    private JTextField engineCapacity;
    private JTextField price;
    private JButton btnCreate;
    private JTextArea textArea;

    public View() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5,5,5,5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblYear = new JLabel("Year: ");
        lblYear.setBounds(10, 11, 100,20);
        contentPane.add(lblYear);

        year = new JTextField();
        year.setBounds(10, 31, 100, 20);
        contentPane.add(year);
        year.setColumns(10);

        JLabel lblCapacity = new JLabel("Engine Capacity: ");
        lblCapacity.setBounds(10, 51, 100,20);
        contentPane.add(lblCapacity);

        engineCapacity = new JTextField();
        engineCapacity.setBounds(10, 71, 100, 20);
        contentPane.add(engineCapacity);
        engineCapacity.setColumns(10);

        JLabel lblPrice= new JLabel("Price: ");
        lblPrice.setBounds(10, 91, 100,20);
        contentPane.add(lblPrice);

        price = new JTextField();
        price.setBounds(10, 111, 100, 20);
        contentPane.add(price);
        price.setColumns(10);

        btnCreate = new JButton("Create");
        btnCreate.setBounds(10, 151, 75, 20);
        contentPane.add(btnCreate);

        textArea = new JTextArea();
        textArea.setBounds(150, 30, 250, 140);
        contentPane.add(textArea);

    }

    public void addBtnCreate(ActionListener e) {
        btnCreate.addActionListener(e);
    }

    public int getYear() {
        return Integer.parseInt(year.getText());
    }

    public int getEngineCapacity() {
        return Integer.parseInt(engineCapacity.getText());
    }

    public Double getPrice() {
        return Double.parseDouble(price.getText());
    }

    public JTextArea getTextArea() {
        return this.textArea;
    }
}