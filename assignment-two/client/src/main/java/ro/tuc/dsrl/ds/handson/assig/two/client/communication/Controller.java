package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class Controller {

    private View view;
    private ITaxService taxService;
    private IPriceService priceService;

    public Controller() throws IOException, NotBoundException {
        view = new View();
        taxService = (ITaxService) Naming.lookup("rmi://localhost:1900" + "/tax");
        priceService = (IPriceService) Naming.lookup("rmi://localhost:1900" + "/price");
        view.setVisible(true);
        view.addBtnCreate(new CreateCar());
    }

    class CreateCar implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            Car car = new Car(view.getYear(), view.getEngineCapacity(), view.getPrice());
            try {
                view.getTextArea().setText("Tax value: " + taxService.computeTax(car) + "\nSelling price: " + priceService.computePrice(car));
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
    }
}
