package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PriceService
        extends UnicastRemoteObject
        implements IPriceService {

    public PriceService() throws RemoteException {
        super();
    }


    @Override
    public double computePrice(Car car) {

        int age = 2018 - car.getYear();
        if (age < 7) {
            return car.getPrice() - car.getPrice()/7 * age;
        }
        return 0;
    }
}
