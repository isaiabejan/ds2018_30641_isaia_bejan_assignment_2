package ro.tuc.dsrl.ds.handson.assig.two.server.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.server.services.PriceService;
import ro.tuc.dsrl.ds.handson.assig.two.server.services.TaxService;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 24, 2015
 * @Description:
 *	Thread which listens for incoming connections and creates a Session for each client.
 */
public class Server implements Runnable {
	private static final Log LOGGER = LogFactory.getLog(Server.class);

	private ServerSocket serverSocket;

	/**
	 * Create a socket object from the ServerSocket to listen to and accept
	 * connections.
	 */
	public Server(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		LocateRegistry.createRegistry(1900);

		ITaxService taxService = new TaxService();
		Naming.rebind("rmi://localhost:1900" + "/tax", taxService);

		IPriceService priceService = new PriceService();
		Naming.rebind("rmi://localhost:1900" + "/price", priceService);

		new Thread(this).start();
	}

	/**
	 * Accepts connections from clients and assigns a thread to deal with the messages form and to the client
	 */
	public void run() {
		while (true) {
			synchronized (this) {

			}
		}
	}
}